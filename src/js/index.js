import "babel-polyfill";
import axios from "axios";
import Vue from "vue";
import Module01 from "./modules/Module01.js";
import App from "./modules/App.vue";
import MonthlyCalendar from "./modules/MonthlyCalendar.vue";

const module01 = new Module01("Taro");

try {
  const component = () => {
    const element = document.createElement("div");

    element.textContent = "Hello World!";

    return element;
  };

  document.body.appendChild(component());
} catch (error) {
  console.error(error);
}

try {
  console.log(module01.greet());
} catch (error) {
  console.error(error);
}

try {
  new Vue({
    el: "#vue-app",
    render: h => h(App)
  });
} catch (error) {
  console.error(error);
}

try {
  axios.get("./sample-01.json").then(response => {
    console.log(response.data);
  });
} catch (error) {
  console.error(error);
}

try {
  new Vue({
    el: "#monthly-calendar",
    render: h => h(MonthlyCalendar)
  });
} catch (error) {
  console.error(error);
}
