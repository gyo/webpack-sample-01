import Module01 from "./modules/Module01.js";

const module01 = new Module01("Taro");

try {
  console.log(module01.greet());
} catch (error) {
  console.error(error);
}
