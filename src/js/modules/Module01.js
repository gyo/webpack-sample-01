/**
 * サンプルのクラス
 */
export default class Module01 {
  /**
   * @param {string} name 名前
   */
  constructor(name) {
    this.name = name;
  }

  /**
   * @returns {string} 挨拶の文字列
   */
  greet() {
    return `Hello! My name is ${this.name}`;
  }
}
