import ObjectUtil from "./ObjectUtil.js";

test("flatten", () => {
  const currentDate = new Date();
  const sampleFunction = () => {
    console.log("Hello");
  };

  const unflattened = {
    a: "A",
    b: {
      c: "C",
      d: "D"
    },
    e: ["E1", "E2", "E3"],
    f: currentDate,
    g: {
      h: [
        {
          i: {
            j: "J",
            k: "K"
          }
        },
        sampleFunction,
        currentDate,
        null,
        undefined,
        NaN,
        {
          l: "L"
        },
        ["M1", "M2"]
      ]
    },
    n: {},
    o: [],
    p: true
  };

  const flattened = {
    a: "A",
    "b.c": "C",
    "b.d": "D",
    "e.0": "E1",
    "e.1": "E2",
    "e.2": "E3",
    f: currentDate,
    "g.h.0.i.j": "J",
    "g.h.0.i.k": "K",
    "g.h.1": sampleFunction,
    "g.h.2": currentDate,
    "g.h.3": null,
    "g.h.4": undefined,
    "g.h.5": NaN,
    "g.h.6.l": "L",
    "g.h.7.0": "M1",
    "g.h.7.1": "M2",
    n: {},
    o: [],
    p: true
  };

  expect(ObjectUtil.flatten(unflattened)).toEqual(flattened);
});

test("unflatten", () => {
  const currentDate = new Date();
  const sampleFunction = () => {
    console.log("Hello");
  };

  const unflattened = {
    a: "A",
    b: {
      c: "C",
      d: "D"
    },
    e: ["E1", "E2", "E3"],
    f: currentDate,
    g: {
      h: [
        {
          i: {
            j: "J",
            k: "K"
          }
        },
        sampleFunction,
        currentDate,
        null,
        undefined,
        NaN,
        {
          l: "L"
        },
        ["M1", "M2"]
      ]
    },
    n: {},
    o: [],
    p: true
  };

  const flattened = {
    a: "A",
    "b.c": "C",
    "b.d": "D",
    "e.0": "E1",
    "e.1": "E2",
    "e.2": "E3",
    f: currentDate,
    "g.h.0.i.j": "J",
    "g.h.0.i.k": "K",
    "g.h.1": sampleFunction,
    "g.h.2": currentDate,
    "g.h.3": null,
    "g.h.4": undefined,
    "g.h.5": NaN,
    "g.h.6.l": "L",
    "g.h.7.0": "M1",
    "g.h.7.1": "M2",
    n: {},
    o: [],
    p: true
  };

  expect(ObjectUtil.unflatten(flattened)).toEqual(unflattened);
});
