import Module01 from "./Module01.js";

test("Module01.greet()", () => {
  const name = "Taro";
  const module01 = new Module01(name);

  expect(module01.greet()).toBe(`Hello! My name is ${name}`);
});
