/**
 * オブジェクト操作メソッド
 */
export default class ObjectUtil {
  /**
   * オブジェクトのキーを flat にする
   * @param {*} obj 元となるオブジェクト
   * @returns {Object} オブジェクト
   */
  static flatten(obj) {
    const isPrimitive = entry => {
      return (
        entry === null ||
        entry === undefined ||
        (!Array.isArray(entry) && entry.constructor !== Object) ||
        ((Array.isArray(entry) && entry.length === 0) || Object.keys(entry).length === 0)
      );
    };

    const getNewKey = (key, nextKey) => {
      return key + "." + nextKey;
    };

    const getFlattenedKeyValue = (key, value) => {
      if (isPrimitive(value)) {
        return { [key]: value };
      }

      const nextKeys = Object.keys(value);

      return nextKeys.reduce((acc, k) => {
        const v = value[k];
        const newKey = getNewKey(key, k);
        const keyValue = getFlattenedKeyValue(newKey, v);

        return Object.assign({}, acc, keyValue);
      }, {});
    };

    return Object.keys(obj).reduce((acc, key) => {
      const value = obj[key];
      const keyValue = getFlattenedKeyValue(key, value);

      return Object.assign(acc, keyValue);
    }, {});
  }

  /**
   * flatにしたオブジェクトのキーを復元する
   * https://stackoverflow.com/questions/42694980/how-to-unflatten-a-javascript-object-in-a-daisy-chain-dot-notation-into-an-objec
   * @param {*} data 元となるオブジェクト
   * @returns {Object} オブジェクト
   */
  static unflatten(data) {
    var result = {};
    for (var key in data) {
      var keys = key.split(".");
      keys.reduce(function(acc, e, j) {
        if (acc && acc[e]) {
          // なにもしない
        } else {
          if (isNaN(Number(keys[j + 1]))) {
            if (keys.length - 1 == j) {
              acc[e] = data[key];
            } else {
              acc[e] = {};
            }
          } else {
            acc[e] = [];
          }
        }
        return acc[e];
      }, result);
    }
    return result;
  }
}
