module.exports = ctx => {
  return {
    plugins: {
      "postcss-import": true,
      "postcss-functions": {
        functions: {
          vw(itemWidth, viewWidth = 750) {
            return `${100 * itemWidth / viewWidth}vw`;
          }
        }
      },
      "postcss-cssnext": {
        features: {
          autoprefixer: false
        }
      },
      autoprefixer: {
        grid: true
      },
      "postcss-style-guide": ctx.env === "production" ? { dest: "./styleguide/index.html" } : false,
      cssnano: ctx.env === "production" ? { autoprefixer: false } : false
    }
  };
};
