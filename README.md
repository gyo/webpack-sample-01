# webpack-sample-01

PostCSS, Vue.js を使う開発環境の構築

# 開発

インストール

```
$ npm install
```

開発

```
$ npm run dev
```

ビルド

```
$ npm run prod
```

| npm スクリプト | 説明                                                                                                                           |
| -------------- | ------------------------------------------------------------------------------------------------------------------------------ |
| test           | Jest を実行                                                                                                                    |
| eslint         | ESLint を実行                                                                                                                  |
| eslintfix      | fix オプション付きで ESLint を実行                                                                                             |
| format         | Prettier を実行                                                                                                                |
| fix            | fix オプション付きで ESLint を実行したのち、Prettier を実行                                                                    |
| styleguide     | CSS のスタイルガイドを生成                                                                                                     |
| build:js       | JS を Development ビルド                                                                                                       |
| build:css      | CSS を Development ビルド                                                                                                      |
| build          | CSS と JS を Development ビルド                                                                                                |
| dev:js         | watch オプション付きで JS を Development ビルド                                                                                |
| dev:css        | watch オプション付きで CSS を Development ビルド                                                                               |
| dev            | watch オプション付きで CSS と JS を Development ビルド                                                                         |
| prod:js        | JS を Production ビルド                                                                                                        |
| prod:css       | CSS を Production ビルド                                                                                                       |
| prod           | `fix`, `test` を順次実行したのち、CSS と JS を Production ビルド。`fix`, `test` でエラーが発生した場合、ビルドは実行されません |

# ライブラリ

## CSS

[PostCSS - a tool for transforming CSS with JavaScript](https://postcss.org/)

これまでは主に Sass を使っていたが、Sass を使う場合、node-sass と autoprefixer の併用によってビルドまわりの設定が複雑化するという問題があった。

autoprefixer との相性の良さ、webpack からの独立性の高さなどを考慮し、PostCSS を採用した。

乗り換えのコストは懸念されるものの、基本となる import 構文や変数宣言は似ているため、大きなコストにはならないと判断した。

| パッケージ名        | 説明                                                          |
| ------------------- | ------------------------------------------------------------- |
| postcss-cli         | CLI                                                           |
| postcss-cssnext     | 最新の構文を使うための PostCSS プラグイン                     |
| postcss-functions   | CSS 内部で JS の関数を利用するための PostCSS プラグイン       |
| postcss-import      | ファイルの分割、結合をするための PostCSS プラグイン           |
| postcss-style-guide | スタイルガイドを生成するための PostCSS プラグイン             |
| autoprefixer        | ベンダープレフィックスを自動付与するための PostCSS プラグイン |
| cssnano             | CSS を minify するための PostCSS プラグイン                   |

## JS

### webpack

[webpack](https://webpack.js.org/)

あくまで JS ファイルのバンドラーとして、薄く使うようにしてください。

| パッケージ名            | 説明                                                     |
| ----------------------- | -------------------------------------------------------- |
| webpack                 | 本体                                                     |
| webpack-cli             | CLI                                                      |
| webpack-bundle-analyzer | バンドルしているパッケージのサイズを可視化するプラグイン |

### Babel

[Babel · The compiler for next generation JavaScript](https://babeljs.io/)

| パッケージ名     | 説明                                                                                                                                                                                  |
| ---------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| babel-core       | 本体                                                                                                                                                                                  |
| babel-loader     | webpack で Babal を扱うためのプラグイン                                                                                                                                               |
| babel-polyfill   | [regenerator runtime](https://github.com/facebook/regenerator/blob/master/packages/regenerator-runtime/runtime.js) と [core-js](https://github.com/zloirock/core-js) を含むポリフィル |
| babel-preset-env | Babel が対応する ES2015+ のバージョンを自動で指定するプラグイン                                                                                                                       |

### Vue.js

[Vue.js](https://jp.vuejs.org/index.html)

導入にあたり [React](https://reactjs.org/) も検討したが、過去に使用実績がありノウハウがたまっていることから、Vue.js を採用した。

| パッケージ名          | 説明                                                                                                         |
| --------------------- | ------------------------------------------------------------------------------------------------------------ |
| vue                   | 本体                                                                                                         |
| vue-loader            | webpack で Vue.js の [SFC](https://jp.vuejs.org/v2/guide/single-file-components.html) を扱うためのプラグイン |
| vue-template-compiler | webpack で Vue.js の [SFC](https://jp.vuejs.org/v2/guide/single-file-components.html) を扱うためのプラグイン |

### 非同期通信

[axios/axios: Promise based HTTP client for the browser and node.js](https://github.com/axios/axios)

| パッケージ名 | 説明 |
| ------------ | ---- |
| axios        | 本体 |

### 日時操作

[Moment.js | Home](https://momentjs.com/)

[Day.js](https://github.com/iamkun/dayjs) も検討したが、isoWeek に対応しており月曜始まりの処理を簡単にできる Moment.js を採用した。

| パッケージ名 | 説明 |
| ------------ | ---- |
| moment       | 本体 |

### バリデーション

[chriso/validator.js: String validation](https://github.com/chriso/validator.js)

導入にあたり [validatorjs](https://github.com/skaterdav85/validatorjs) も検討したが、非同期のバリデーションを扱いにくかったため見送り。

Validator を自作することも検討したが、パラメータが可変長の場合や複数パラメータを考慮する必要がある場合にルール定義やバリデーション結果オブジェクトの型が複雑になりすぎるといった理由から見送り。

バリデーション自体はコンポーネントごとに書くこととし、メソッド集として validator.js を採用した。

| パッケージ名 | 説明 |
| ------------ | ---- |
| validator    | 本体 |

## その他

[ESLint - Pluggable JavaScript linter](https://eslint.org/)

[Jest · 🃏 快適な JavaScript のテスト](https://facebook.github.io/jest/ja/)

[Prettier · Opinionated Code Formatter](https://prettier.io/)

[mysticatea/npm-run-all: A CLI tool to run multiple npm-scripts in parallel or sequential.](https://github.com/mysticatea/npm-run-all)

| パッケージ名       | 説明                                                        |
| ------------------ | ----------------------------------------------------------- |
| eslint             | JavaScript の Lint ツール                                   |
| eslint-plugin-jest | Jest のコードで ESLint を動作させるためのプラグイン         |
| eslint-plugin-vue  | Vue.js のコードで ESLint を動作させるためのプラグイン       |
| jest               | javaScript のテストプラットフォーム                         |
| prettier           | CSS, JS などのフォーマッター                                |
| npm-run-all        | 複数の npm スクリプトの同時実行を書きやすくするためのツール |
